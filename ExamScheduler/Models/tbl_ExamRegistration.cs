//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExamScheduler.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ExamRegistration
    {
        public int PK_ID { get; set; }
        public Nullable<int> FK_Exam { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<System.DateTime> Time { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string VoucherNumber { get; set; }
    
        public virtual tbl_Exam tbl_Exam { get; set; }
    }
}
