﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamScheduler.Models;

namespace ExamScheduler.Controllers
{
    public class ExamController : Controller
    {
        private db_ExamSchedulerEntities1 db = new db_ExamSchedulerEntities1();

        // GET: Exam
        public ActionResult Index()
        {
            var tbl_Exam = db.tbl_Exam.Include(t => t.tbl_ExamGroup).Include(t => t.tbl_Schedule);
            return View(tbl_Exam.ToList());
        }

        // GET: Exam/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Exam tbl_Exam = db.tbl_Exam.Find(id);
            if (tbl_Exam == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Exam);
        }

        // GET: Exam/Create
        public ActionResult Create()
        {
            ViewBag.FK_ExamGroup = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName");
            ViewBag.FK_Schedule = db.tbl_Schedule.ToList();
            return View();
        }

        // POST: Exam/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_ID,ExamName,FK_ExamGroup,IsStudentReschedule,IsShowStudentReschedule,FK_Schedule,AllottedMinutes,CollegeFee,ReTakeFee,Description,Acknowledgement")] tbl_Exam tbl_Exam)
        {
            if (tbl_Exam.FK_ExamGroup != 0)
            {
                var examgoup = db.tbl_ExamGroup.Find(tbl_Exam.FK_ExamGroup);
                tbl_Exam.FK_Schedule = tbl_Exam.FK_Schedule == null ?examgoup.FK_Schedule: tbl_Exam.FK_Schedule;
                tbl_Exam.IsStudentReschedule = tbl_Exam.IsStudentReschedule == null ? examgoup.AllowReschedule : tbl_Exam.IsStudentReschedule;
                //tbl_Exam.is = tbl_Exam.FK_Schedule == null ? examgoup.FK_Schedule : tbl_Exam.FK_Schedule;
            }
            if (ModelState.IsValid)
            {
                db.tbl_Exam.Add(tbl_Exam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_ExamGroup = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName", tbl_Exam.FK_ExamGroup);
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_Exam.FK_Schedule);
            return View(tbl_Exam);
        }

        // GET: Exam/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Exam tbl_Exam = db.tbl_Exam.Find(id);
            if (tbl_Exam == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_ExamGroup = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName", tbl_Exam.FK_ExamGroup);
            ViewBag.FK_Schedule = db.tbl_Schedule.ToList();
            return View(tbl_Exam);
        }

        // POST: Exam/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_ID,ExamName,FK_ExamGroup,IsStudentReschedule,IsShowStudentReschedule,FK_Schedule,AllottedMinutes,CollegeFee,ReTakeFee,Description,Acknowledgement")] tbl_Exam tbl_Exam)
        {
            if (tbl_Exam.FK_ExamGroup != 0)
            {
                var examgoup = db.tbl_ExamGroup.Find(tbl_Exam.FK_ExamGroup);
                tbl_Exam.FK_Schedule = tbl_Exam.FK_Schedule == null ? examgoup.FK_Schedule : tbl_Exam.FK_Schedule;
                tbl_Exam.IsStudentReschedule = tbl_Exam.IsStudentReschedule == null ? examgoup.AllowReschedule : tbl_Exam.IsStudentReschedule;
                //tbl_Exam.is = tbl_Exam.FK_Schedule == null ? examgoup.FK_Schedule : tbl_Exam.FK_Schedule;
            }
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Exam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_ExamGroup = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName", tbl_Exam.FK_ExamGroup);
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_Exam.FK_Schedule);
            return View(tbl_Exam);
        }

        // GET: Exam/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Exam tbl_Exam = db.tbl_Exam.Find(id);
            if (tbl_Exam == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Exam);
        }

        // POST: Exam/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Exam tbl_Exam = db.tbl_Exam.Find(id);
            db.tbl_Exam.Remove(tbl_Exam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
