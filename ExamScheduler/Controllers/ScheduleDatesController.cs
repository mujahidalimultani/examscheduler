﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamScheduler.Models;

namespace ExamScheduler.Controllers
{
    public class ScheduleDatesController : Controller
    {
        private db_ExamSchedulerEntities1 db = new db_ExamSchedulerEntities1();

        // GET: ScheduleDates
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tbl_ScheduleDates = db.tbl_ScheduleDates.Where(x=>x.FK_Schedule==id).Include(t => t.tbl_Schedule);
            ViewBag.Schedule = db.tbl_Schedule.Find(id);
            return View(tbl_ScheduleDates.ToList());
        }


        [HttpPost]
        public bool MultiOpen(string Ids)
        {
            try
            {
                string[] idArr = Ids.Split(',');
                foreach (var item in idArr)
                {
                    if (int.TryParse(item, out int result))
                    {
                        var e = db.Database.ExecuteSqlCommand("update tbl_ScheduleDates set Closed=0 where PK_ID=@Id", new SqlParameter("@Id", result));
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }

        }


        [HttpPost]
        public bool MultiClose(string Ids)
        {
            try
            {
                string[] idArr = Ids.Split(',');
                foreach (var item in idArr)
                {
                    if (int.TryParse(item, out int result))
                    {
                        var e= db.Database.ExecuteSqlCommand("update tbl_ScheduleDates set Closed=1 where PK_ID=@Id", new SqlParameter("@Id", result));
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }

        }


        [HttpPost]
        public bool MultiDelete(string Ids)
        {
            try
            {
                string[] idArr = Ids.Split(',');
                foreach (var item in idArr)
                {
                    if (int.TryParse(item, out int result))
                    {
                        tbl_ScheduleDates tbl_ScheduleDates = db.tbl_ScheduleDates.Find(result);
                        db.tbl_ScheduleDates.Remove(tbl_ScheduleDates);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
            
        }




        [HttpPost]
        public ActionResult CreateDates(
            int? FK_Schedule,
            Nullable<System.DateTime> dateFrom,
            Nullable<System.DateTime> dateTo,
            string repeatEvery,
            string repeatEveryOn,
            Nullable<System.DateTime> allow_timeFrom,
            Nullable<System.DateTime> allow_timeTo,
            bool? day1=false,
            bool? day2 = false,
            bool? day3 = false,
            bool? day4 = false,
            bool? day5 = false,
            bool? day6 = false,
            bool? day7 = false,
            bool? chkClosedDates=false,
            int? Seats=null,
            string totalSlots=null
            )
        {

            dateFrom =(dateFrom<DateTime.Now)? DateTime.Now:dateFrom;


            for (var day = dateFrom.Value.Date; day <= dateTo.Value.Date; day = day.AddDays(1))
            {

                if (
                    (day1 == true && day.DayOfWeek == DayOfWeek.Sunday)||
                    (day2 == true && day.DayOfWeek == DayOfWeek.Monday) ||
                    (day3 == true && day.DayOfWeek == DayOfWeek.Tuesday) || 
                    (day4 == true && day.DayOfWeek == DayOfWeek.Wednesday) ||
                    (day5 == true && day.DayOfWeek == DayOfWeek.Thursday) ||
                    (day6 == true && day.DayOfWeek == DayOfWeek.Friday) ||
                    (day7 == true && day.DayOfWeek == DayOfWeek.Saturday)
                    )
                {
                    if (totalSlots != null)
                    {

                        string[] times = totalSlots.Split(',');

                        foreach (var x in times)
                        {
                            string xx= x.TrimStart().TrimEnd();
                            bool isTime = true;
                            TimeSpan result = DateTime.Now.TimeOfDay;
                            try
                            {
                                DateTime dt = DateTime.ParseExact(xx, "h:mm tt", CultureInfo.InvariantCulture);
                                result = dt.TimeOfDay;
                            }
                            catch
                            {
                                isTime = false;
                            }

                            if (isTime)
                            {
                                DateTime dateTime = new DateTime(day.Year, day.Month, day.Day, result.Hours, result.Minutes, result.Seconds);

                                db.tbl_ScheduleDates.Add(new tbl_ScheduleDates()
                                {
                                    Closed = chkClosedDates ?? false,
                                    Date = dateTime,
                                    FK_Schedule = FK_Schedule,
                                    FromTime = allow_timeFrom,
                                    ToTime = allow_timeTo,
                                    Seats = Seats ?? 0,
                                    Status = true
                                });
                                db.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        db.tbl_ScheduleDates.Add(new tbl_ScheduleDates()
                        {
                            Closed = chkClosedDates ?? false,
                            Date = day,
                            FK_Schedule = FK_Schedule,
                            FromTime = allow_timeFrom,
                            ToTime = allow_timeTo,
                            Seats = Seats ?? 0,
                            Status = true
                        });
                    }
                }
                db.SaveChanges();

            }
            return RedirectToAction("Index", new { id = FK_Schedule });
        }





        // GET: ScheduleDates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ScheduleDates tbl_ScheduleDates = db.tbl_ScheduleDates.Find(id);
            if (tbl_ScheduleDates == null)
            {
                return HttpNotFound();
            }
            return View(tbl_ScheduleDates);
        }

        // GET: ScheduleDates/Create
        public ActionResult Create()
        {
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName");
            return View();
        }

        // POST: ScheduleDates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_ID,FK_Schedule,Date,Seats,Closed,Status,FromTime,ToTime")] tbl_ScheduleDates tbl_ScheduleDates)
        {
            if (ModelState.IsValid)
            {
                db.tbl_ScheduleDates.Add(tbl_ScheduleDates);
                db.SaveChanges();
                return RedirectToAction("Index",new { id=tbl_ScheduleDates.FK_Schedule });
            }

            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_ScheduleDates.FK_Schedule);
            return View(tbl_ScheduleDates);
        }

        // GET: ScheduleDates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ScheduleDates tbl_ScheduleDates = db.tbl_ScheduleDates.Find(id);
            if (tbl_ScheduleDates == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_ScheduleDates.FK_Schedule);
            return View(tbl_ScheduleDates);
        }

        // POST: ScheduleDates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_ID,FK_Schedule,Date,Seats,Closed,Status,FromTime,ToTime")] tbl_ScheduleDates tbl_ScheduleDates)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_ScheduleDates).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = tbl_ScheduleDates.FK_Schedule });
            }
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_ScheduleDates.FK_Schedule);
            return View(tbl_ScheduleDates);
        }

        // GET: ScheduleDates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ScheduleDates tbl_ScheduleDates = db.tbl_ScheduleDates.Find(id);
            if (tbl_ScheduleDates == null)
            {
                return HttpNotFound();
            }
            return View(tbl_ScheduleDates);
        }

        // POST: ScheduleDates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_ScheduleDates tbl_ScheduleDates = db.tbl_ScheduleDates.Find(id);
            var scheduleId = tbl_ScheduleDates.FK_Schedule;
            db.tbl_ScheduleDates.Remove(tbl_ScheduleDates);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = scheduleId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
