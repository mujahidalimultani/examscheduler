﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExamScheduler.Models;

namespace ExamScheduler.Controllers
{
    public class ExamsController : Controller
    {
        db_ExamSchedulerEntities1 db;
        public ExamsController()
        {
            db = new db_ExamSchedulerEntities1();
        }
        public ActionResult List()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetExamsGroup()
        {
            var group = db.tbl_ExamGroup.Where(x=>x.FK_ExamGroupParent==null).ToList();
            return Json(group.Select(x => new
            {
                PK_ID = x.PK_ID,
                ExamGroupName = x.ExamGroupName,
                HasChild=db.tbl_ExamGroup.Any(y=>y.FK_ExamGroupParent==x.PK_ID)
            }),JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetChildExamsGroup(int id)
        {
            var group = db.tbl_ExamGroup.Where(x => x.FK_ExamGroupParent == id).ToList();
            return Json(group.Select(x => new
            {
                PK_ID = x.PK_ID,
                ExamGroupName = x.ExamGroupName,
            }), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetExams(int id)
        {
            var group = db.tbl_Exam.Where(x=>x.FK_ExamGroup==id).ToList();
            return Json(group.Select(x => new
            {
                PK_ID = x.PK_ID,
                ExamName = x.ExamName,
                Fee=x.CollegeFee
            }), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetExamDates(int id)
        {
            var exam = db.tbl_Exam.Find(id);
            var dates = db.tbl_ScheduleDates.Where(x => x.FK_Schedule == exam.FK_Schedule).Select(x=>new { Date= x.Date.Value }).ToArray();
            List<object> list = new List<object>();
            foreach (var item in dates)
            {
                var date = item.Date.ToString("d/M/yyyy");
                list.Add(date);
            }

            var minDate = dates.Min(x=>x.Date).Date.ToString("yyyy-MM-dd");
            var maxDate = dates.Max(x=>x.Date).Date.ToString("yyyy-MM-dd");
            var json = new
            {
                dates = list.ToArray(),
                minDate,
                maxDate
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetExamTimes(int id,DateTime date)
        {
            var exam = db.tbl_Exam.Find(id);
            var dates = db.tbl_ScheduleDates.Where(x => x.FK_Schedule == exam.FK_Schedule&&DbFunctions.TruncateTime(x.Date)==date).ToList();
            List<object> times = new List<object>();
            dates.ForEach((v) =>
            {
                //December 25, 1995 23:15:20
                times.Add(new {
                    PK_ID=v.PK_ID,
                    TimeFrom=v.FromTime.Value.ToString("MMM dd, yyyy hh:mm:ss tt"),
                    TimeTo = v.ToTime.Value.ToString("MMM dd, yyyy hh:mm:ss tt"),
                });
            });

            return Json(new
            {
                times
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddToCart(Models.tbl_ExamRegistration registration)
        {
            if (ModelState.IsValid)
            {
                db.tbl_ExamRegistration.Add(registration);
                db.SaveChanges();

                return Content("Registered");
            }
            return View("List");
        }
    }
}