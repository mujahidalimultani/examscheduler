﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamScheduler.Models;

namespace ExamScheduler.Controllers
{
    public class ScheduleController : Controller
    {
        private db_ExamSchedulerEntities1 db = new db_ExamSchedulerEntities1();

        // GET: Schedule
        public ActionResult Index()
        {
            return View(db.tbl_Schedule.ToList());
        }

        // GET: Schedule/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Schedule tbl_Schedule = db.tbl_Schedule.Find(id);
            if (tbl_Schedule == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Schedule);
        }

        // GET: Schedule/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Schedule/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_ID,ScheduleName,BecomeAvaiableDay,BecomeUnavaiableDay,Archive,IgnoreSeatLimit,BlockOrHours")] tbl_Schedule tbl_Schedule)
        {
            if (ModelState.IsValid)
            {
                db.tbl_Schedule.Add(tbl_Schedule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_Schedule);
        }

        // GET: Schedule/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Schedule tbl_Schedule = db.tbl_Schedule.Find(id);
            if (tbl_Schedule == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Schedule);
        }

        // POST: Schedule/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_ID,ScheduleName,BecomeAvaiableDay,BecomeUnavaiableDay,Archive,IgnoreSeatLimit,BlockOrHours")] tbl_Schedule tbl_Schedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_Schedule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_Schedule);
        }

        // GET: Schedule/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_Schedule tbl_Schedule = db.tbl_Schedule.Find(id);
            if (tbl_Schedule == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Schedule);
        }

        // POST: Schedule/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_Schedule tbl_Schedule = db.tbl_Schedule.Find(id);
            db.tbl_Schedule.Remove(tbl_Schedule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
