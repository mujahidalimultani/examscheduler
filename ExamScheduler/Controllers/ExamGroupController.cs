﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamScheduler.Models;

namespace ExamScheduler.Controllers
{
    public class ExamGroupController : Controller
    {
        private db_ExamSchedulerEntities1 db = new db_ExamSchedulerEntities1();

        // GET: ExamGroup
        public ActionResult Index()
        {
            var tbl_ExamGroup = db.tbl_ExamGroup.Include(t => t.tbl_ExamGroup2).Include(t => t.tbl_Schedule);
            return View(tbl_ExamGroup.ToList());
        }

        // GET: ExamGroup/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ExamGroup tbl_ExamGroup = db.tbl_ExamGroup.Find(id);
            if (tbl_ExamGroup == null)
            {
                return HttpNotFound();
            }
            return View(tbl_ExamGroup);
        }

        // GET: ExamGroup/Create
        public ActionResult Create()
        {
            ViewBag.FK_ExamGroupParent = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName");
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName");
            return View();
        }

        // POST: ExamGroup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_ID,ExamGroupName,FK_ExamGroupParent,FK_Schedule,AllowWalkInRegistration,PriorityNumber,ExamFee,CancelRescheduleFee,SeatLimite,CancelRescheduleDays,AllowReschedule,Acknowledgement")] tbl_ExamGroup tbl_ExamGroup)
        {
            if (ModelState.IsValid)
            {
                db.tbl_ExamGroup.Add(tbl_ExamGroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_ExamGroupParent = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName", tbl_ExamGroup.FK_ExamGroupParent);
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_ExamGroup.FK_Schedule);
            return View(tbl_ExamGroup);
        }

        // GET: ExamGroup/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ExamGroup tbl_ExamGroup = db.tbl_ExamGroup.Find(id);
            if (tbl_ExamGroup == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_ExamGroupParent = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName", tbl_ExamGroup.FK_ExamGroupParent);
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_ExamGroup.FK_Schedule);
            return View(tbl_ExamGroup);
        }

        // POST: ExamGroup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_ID,ExamGroupName,FK_ExamGroupParent,FK_Schedule,AllowWalkInRegistration,PriorityNumber,ExamFee,CancelRescheduleFee,SeatLimite,CancelRescheduleDays,AllowReschedule,Acknowledgement")] tbl_ExamGroup tbl_ExamGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_ExamGroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_ExamGroupParent = new SelectList(db.tbl_ExamGroup, "PK_ID", "ExamGroupName", tbl_ExamGroup.FK_ExamGroupParent);
            ViewBag.FK_Schedule = new SelectList(db.tbl_Schedule, "PK_ID", "ScheduleName", tbl_ExamGroup.FK_Schedule);
            return View(tbl_ExamGroup);
        }

        // GET: ExamGroup/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ExamGroup tbl_ExamGroup = db.tbl_ExamGroup.Find(id);
            if (tbl_ExamGroup == null)
            {
                return HttpNotFound();
            }
            return View(tbl_ExamGroup);
        }

        // POST: ExamGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_ExamGroup tbl_ExamGroup = db.tbl_ExamGroup.Find(id);
            db.tbl_ExamGroup.Remove(tbl_ExamGroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
